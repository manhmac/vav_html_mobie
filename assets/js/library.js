// menu
$(document).ready(function(){
  $('.btn-toggle-menu').click(function(e){
    e.preventDefault(); 
    e.stopPropagation()
    $('.menu').toggleClass('active');
    $('.btn-toggle-menu').toggleClass('hambger');
    $('body').toggleClass('bgr-op');
    $('body').toggleClass('ofl');
  })
  $('.menu').click( function(e) {
    e.stopPropagation(); 
  });
    
  $('body').click( function() {
    $('.menu').removeClass('active');
    $('.btn-toggle-menu').removeClass('hambger')
    $('body').removeClass('bgr-op');
    $('body').removeClass('ofl');
  });

// multil menu
  $('.dropdown-toggle').on('click', function(e) {
    if (!$(this).next().hasClass('show')) {
      $(this).parents('.dropdown-menu').first().find('.show').removeClass('show');
    }
    var $subMenu = $(this).next('.dropdown-menu');
    $subMenu.toggleClass('show');
    $(this).parents('li.nav-item.dropdown.show').on('hidden.bs.dropdown', function(e) {
      $('.dropdown-submenu .show').removeClass('show');
    });
    return false;
  });


   // fix-menu 
  var section = $('header');
  var start = $(section).offset().top;
  $.event.add(window, "scroll", function () {
    var img = $('.logo');
    var p = $(window).scrollTop(); 
    $(section).css('position', ((p) > start) ? 'fixed' : 'relative'); 
    $(section).css('top', ((p) > start) ? '0px' : '');
    $(section).css('width', ((p) > start) ? '100%' : '');
    $(section).css('box-shadow', ((p) > start) ? '0 2px 2px rgba(0, 0, 0, 0.07)' : '');
    if (p <= 0) { 
      $(section).removeClass('scrollHeader');
      $(".content-main").css('margin-top','0');
      $(".fix-index .box-search").show();
      $(".header-tab2").next().css('margin-top','0');

    } else { 
      $(section).addClass('scrollHeader');
      $(".content-main").css('margin-top','59px'); 
      $(".fix-index .box-search").hide();
      $(".header-tab2").next().css('margin-top','38px');
    }
  });


});

// ...


    

// SLIDE img detail product
$('.box-slide-img-prod').slick({
  dots: true,
  infinite: true,
  speed: 500,
  fade: true,
  cssEase: 'linear',
  autoplay: true,
  autoplaySpeed: 4000,
  height:800,
  arrows:false,
});
// .




// slick slide product other
$('.list-other-prod').slick({
  slidesToShow: 2,
  slidesToScroll: 1,
  dots: false,
  focusOnSelect: true,
 
});
//slide banner
$('.slide-img-top').slick({
  slidesToShow: 2,
  slidesToScroll: 1,
  dots: false,
  focusOnSelect: true,
});
//slide introduce
$('.list-img-cty').slick({
  slidesToShow: 1,
  slidesToScroll: 1,
  dots: true,
  focusOnSelect: true,
  arrows:false,
});


// tabs
function prod(evt, tabprod) {
  var i, tabcontent, tablinks;
  tabcontent = document.getElementsByClassName("tabcontent");
  for (i = 0; i < tabcontent.length; i++) {
    tabcontent[i].style.display = "none";
  }
  tablinks = document.getElementsByClassName("tablinks");
  for (i = 0; i < tablinks.length; i++) {
    tablinks[i].className = tablinks[i].className.replace(" active", "");
  }
  document.getElementById(tabprod).style.display = "block";
  evt.currentTarget.className += " active";
}

// Back to top
  $(".back-to-top a").click(function (n) {
      n.preventDefault();
      $("html, body").animate({
          scrollTop: 0
      }, 500)
  });
  $(window).scroll(function () {
      $(document).scrollTop() > 1e2 ? $(".back-to-top").addClass("display") : $(".back-to-top").removeClass("display")
  });




// scroll footer
// $(function(){
//   $(window).scroll(function() {  
//     if($(document).scrollTop() > 0)
//     {    
//       $('#footer').addClass("show");
//     }
//     else
//     {
//       $('#footer').removeClass("show");
//     }
//   });
// })




// updown sl sản phẩm
var numbprod = document.getElementById("numb-prod");
var number = 1
function add(value){
    var sum = number + value;
    if (sum > 0 && sum < 6 ) {
        number = sum;
    }
    numbprod.innerHTML = number;
}

